let banners = document.querySelectorAll(".image-to-show");
let stopButton = document.querySelector(".stopButton");
let resumeButton = document.querySelector(".resumeButton");
let bannerIndex = 0;

stopButton.style.display = "none";
resumeButton.style.display = "none";


for( let i = 0; i < banners.length; i++){
    banners[i].style.display = "none";
}

function showBanner(index){

    for( let i = 0; i < banners.length; i++){

        banners[i].style.display = "none";

        if(i === index) {
            banners[i].style.display = "block";
        }
    }

}

function startBanners(){
    showBanner(bannerIndex);
    bannerIndex = (bannerIndex + 1) % banners.length;
    stopButton.style.display = "block";
    resumeButton.style.display = "block";
    resumeButton.disabled = true;
}

let interval = setInterval(startBanners, 3000);

stopButton.addEventListener("click", function(){
    clearInterval(interval);
    resumeButton.disabled = false;
})

resumeButton.addEventListener("click", function(){
    interval = setInterval(startBanners, 3000);
})